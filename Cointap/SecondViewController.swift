//
//  SecondViewController.swift
//  Cointap
//
//  Created by Helder De Baere on 9/12/15.
//  Copyright © 2015 Helder De Baere. All rights reserved.
//

import UIKit
import Foundation

class SecondViewController: UITableViewController {
    
    /* BLOCKS */
    @IBOutlet weak var totalBlocks: UILabel!
    @IBOutlet weak var totalBitcoins: UILabel!
    @IBOutlet weak var percentMined: UILabel!
    /* DIFFICULTY */
    @IBOutlet weak var difficulty: UILabel!
    @IBOutlet weak var nextDifficulty: UILabel!
    @IBOutlet weak var differenceDifficulty: UILabel!
    @IBOutlet weak var blockNrNextDifficulty: UILabel!
    @IBOutlet weak var nrOfBlockNextDifficulty: UILabel!
    /* LAST BLOCK*/
    @IBOutlet weak var blockNumber: UILabel!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var numberTransactions: UILabel!
    @IBOutlet weak var totalFees: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
        self.refreshControl?.tintColor = UIColor.orangeColor()
        self.refreshControl?.addTarget(self, action: Selector("getData"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func getData() {
        let urlString: String = "https://btc.blockr.io/api/v1/coin/info"
        guard let url = NSURL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            guard error == nil else {
                print("error calling GET on /info")
                print(error)
                return
            }
            
            let data: NSDictionary
            do {
                data = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            self.updateUI(data)
            
        })
        task.resume()
    }
    
    func updateUI(data: NSDictionary) {
        let fmt = NSNumberFormatter()
        fmt.numberStyle = .DecimalStyle
        let lastBlockData: NSDictionary = data["data"]!["last_block"] as! NSDictionary
        let nextDifficultyData: NSDictionary = data["data"]!["next_difficulty"] as! NSDictionary
        let volumeData: NSDictionary = data["data"]!["volume"] as! NSDictionary
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.beginUpdates()
            self.totalBlocks.text = String(format: "%d", lastBlockData["nb"]!.integerValue)
            self.totalBitcoins.text = "Ƀ" + fmt.stringFromNumber(volumeData["current"]!.integerValue)!
            self.percentMined.text = String(format:"%.2f%%", volumeData["perc"]!.doubleValue)
            self.difficulty.text = fmt.stringFromNumber(lastBlockData["difficulty"]!.doubleValue)!
            self.nextDifficulty.text = fmt.stringFromNumber(nextDifficultyData["difficulty"]!.doubleValue)!
            self.differenceDifficulty.text = String(format:"%.2f%%", nextDifficultyData["perc"]!.doubleValue)
            self.blockNrNextDifficulty.text = String(format:"%d", nextDifficultyData["retarget_block"]!.integerValue)
            self.nrOfBlockNextDifficulty.text = String(format:"%d blokken", nextDifficultyData["retarget_in"]!.integerValue)
            self.blockNumber.text = String(format: "%d", lastBlockData["nb"]!.integerValue)
            let utcTime: String = lastBlockData["time_utc"] as! String
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            let date = dateFormatter.dateFromString(utcTime)
            dateFormatter.timeZone = NSTimeZone.localTimeZone()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let strDate = dateFormatter.stringFromDate(date!)
            self.timeStamp.text = String(format: "%@", strDate)
            self.numberTransactions.text = String(format: "%d", lastBlockData["nb_txs"]!.integerValue)
            self.totalFees.text = "Ƀ" + String(format: "%f", lastBlockData["fee"]!.doubleValue)
            self.tableView.endUpdates()
            self.tableView.reloadData()
        })
        self.refreshControl?.endRefreshing()
    }
}