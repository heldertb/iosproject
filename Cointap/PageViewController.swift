//
//  PageSegue.swift
//  Cointap
//
//  Created by Helder De Baere on 9/12/15.
//  Copyright © 2015 Helder De Baere. All rights reserved.
//
//  Tutorial for PageViews: http://swiftiostutorials.com/ios-tutorial-using-uipageviewcontroller-create-content-slider-objective-cswift/
//  Also used documentation from Apple developers


import UIKit

public class PageViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var controllers = [MyViewControllers]()
    private var pageViewController: UIPageViewController?
    
    public override func viewDidLoad() {
        for i in 0...2 {
            let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController\(i)") as! MyViewControllers
            controller.itemIndex = i
            controllers.append(controller)
        }
        createPageViewController()
        setupPageControl()
    }
    
    public func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let controller = viewController as? MyViewControllers {
            if controller.itemIndex > 0 {
                return controllers[controller.itemIndex - 1]
            }
        }
        return nil
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.grayColor()
        appearance.currentPageIndicatorTintColor = UIColor.yellowColor()
        appearance.backgroundColor = UIColor.clearColor()
    }
    
    private func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if !controllers.isEmpty {
            pageController.setViewControllers([controllers[0]], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        }
      
        
        pageViewController = pageController
        pageViewController!.view.frame = CGRectMake(0.0, 0.0, view.frame.size.width, view.frame.size.height)
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
    }
    
    public func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let controller = viewController as? MyViewControllers {
            if controller.itemIndex < controllers.count - 1 {
                return controllers[controller.itemIndex + 1]
            }
        }
        return nil
    }
    
    public func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return controllers.count
    }
    
    public func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
}