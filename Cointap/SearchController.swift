//
//  SearchController.swift
//  Cointap
//
//  Created by Helder De Baere on 15/01/16.
//  Copyright © 2016 Helder De Baere. All rights reserved.
//

import Foundation
import UIKit

class SearchController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    let searchController = UISearchController(searchResultsController: nil)
    var items: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.showsScopeBar = true
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        cell.textLabel?.text = self.items[indexPath.row]
        
        return cell
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonPressed(sender: AnyObject) {
        let pasteboardString:String? = UIPasteboard.generalPasteboard().string
        if let content = pasteboardString {
            self.searchBar.text = content
            searchBarSearchButtonClicked(self.searchBar)
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                let alert = UIAlertController(title: "Fout", message: "We konden niets ophalen van je klembord!", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Sluit", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            })
        }
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar)
    {
        self.searchBar.endEditing(true)
        if searchBar.text?.characters.count >= 26 && searchBar.text?.characters.count <= 35 && (searchBar.text?.hasPrefix("1") == true || searchBar.text?.hasPrefix("3") == true) {
            // BITCOIN ADRES
            let urlString: String = "https://blockchain.info/nl/rawaddr/" + searchBar.text!
            guard let url = NSURL(string: urlString) else {
                print("Error: cannot create URL")
                return
            }
            
            let urlRequest = NSURLRequest(URL: url)
            
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: config)
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
                
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                guard error == nil else {
                    print("error calling GET on /ticker")
                    print(error)
                    return
                }
                
                let data: NSDictionary
                do {
                    data = try NSJSONSerialization.JSONObjectWithData(responseData,
                        options: []) as! NSDictionary
                } catch  {
                    print("error trying to convert data to JSON")
                    return
                }
                self.fillAddressInfo(data)
            })
            task.resume()
        } else if let blockNumber = NSNumberFormatter().numberFromString(self.searchBar.text!) {
            // BLOK NUMMER
            let block: String = String(blockNumber)
            let urlString: String = "https://blockchain.info/nl/block-height/\(block)?format=json"
            guard let url = NSURL(string: urlString) else {
                print("Error: cannot create URL")
                return
            }
            
            let urlRequest = NSURLRequest(URL: url)
            
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: config)
            let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
                
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                guard error == nil else {
                    print("error calling GET on /ticker")
                    print(error)
                    return
                }
                
                let data: NSDictionary
                do {
                    data = try NSJSONSerialization.JSONObjectWithData(responseData,
                        options: []) as! NSDictionary
                } catch  {
                    dispatch_async(dispatch_get_main_queue(), {
                        let alert = UIAlertController(title: "Fout", message: "Blok niet gevonden!", preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: "Sluit", style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    })
                    return
                }
                self.fillBlockData(data)
            })
            task.resume()
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                let alert = UIAlertController(title: "Fout", message: "Zoekpatroon niet herkent!", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Sluit", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            })
        }
        
    }
    
    func fillAddressInfo(data: NSDictionary) {
        items = []
        items.append("Adres: " + (data["address"] as! String))
        items.append(String(format: "Aantal Transacties: %d", data["n_tx"]!.integerValue))
        items.append(String(format: "Totaal ontvangen: Ƀ%f", data["total_received"]!.doubleValue / 100000000))
        items.append(String(format: "Totaal verzonden: Ƀ%f", data["total_sent"]!.doubleValue / 100000000))
        items.append(String(format: "Huidige balans: Ƀ%f", data["final_balance"]!.doubleValue / 100000000))
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
        })
    }
    
    func fillBlockData(data: NSDictionary) {
        let blocks: NSArray = data["blocks"] as! NSArray
        let blockData: NSDictionary = blocks[0] as! NSDictionary
        items = []
        items.append(String(format: "Bloknummer: %d", blockData["height"]!.integerValue))
        items.append(String(format: "Transactiekosten: Ƀ%f", blockData["fee"]!.doubleValue / 100000000))
        items.append(String(format: "Versie: %d", blockData["ver"]!.integerValue))
        items.append(String(format: "Grootte: %.2fKB", blockData["size"]!.doubleValue / 1024))
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.reloadData()
        })
    }
}