//
//  FirstViewController.swift
//  Cointap
//
//  Created by Helder De Baere on 9/12/15.
//  Copyright © 2015 Helder De Baere. All rights reserved.
//
//  Tutorial for API Calls: http://grokswift.com/simple-rest-with-swift

import UIKit
import Foundation
import Charts

class FirstViewController: UITableViewController, ChartViewDelegate {
    
    @IBOutlet weak var prijsLabel: UILabel!
    @IBOutlet weak var changeLabel: UILabel!
    @IBOutlet weak var updatedLabel: UILabel!
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var moreButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAveragePrice()
        self.drawChart()
        self.refreshControl?.tintColor = UIColor.orangeColor()
        self.refreshControl?.addTarget(self, action: Selector("getAveragePrice"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func getAveragePrice() -> Double{
        var average: Double = 0.0;
        let bitfinexUrlString: String = "https://api.bitfinex.com/v1/pubticker/BTCUSD"
        guard let bitfinexUrl = NSURL(string: bitfinexUrlString) else {
            print("Error: cannot create URL")
            return -1
        }
        let bitfinixUrlRequest = NSURLRequest(URL: bitfinexUrl)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let bitfinexTask = session.dataTaskWithRequest(bitfinixUrlRequest, completionHandler: { (data, response, error) in
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            guard error == nil else {
                print("error calling GET on bitfinex")
                print(error)
                return
            }
            
            let data: NSDictionary
            do {
                data = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            
            average = data["last_price"]!.doubleValue
            let btceUrlString: String = "https://btc-e.com/api/3/ticker/btc_usd"
            guard let btceUrl = NSURL(string: btceUrlString) else {
                print("Error: cannot create URL")
                return
            }
            let btceUrlRequest = NSURLRequest(URL: btceUrl)
            let btceTask = session.dataTaskWithRequest(btceUrlRequest, completionHandler: { (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                guard error == nil else {
                    print("error calling GET on bitfinex")
                    print(error)
                    return
                }
                
                let data: NSDictionary
                do {
                    data = try NSJSONSerialization.JSONObjectWithData(responseData,
                        options: []) as! NSDictionary
                    average = (average + (data["btc_usd"]!["avg"]!!.doubleValue))/2
                } catch  {
                    print("error trying to convert data to JSON")
                    return
                }
                let urlString: String = "https://www.bitstamp.net/api/ticker/"
                guard let url = NSURL(string: urlString) else {
                    print("Error: cannot create URL")
                    return
                }
                let urlRequest = NSURLRequest(URL: url)
                
                let config = NSURLSessionConfiguration.defaultSessionConfiguration()
                let session = NSURLSession(configuration: config)
                let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
                    
                    guard let responseData = data else {
                        print("Error: did not receive data")
                        return
                    }
                    guard error == nil else {
                        print("error calling GET on /ticker")
                        print(error)
                        return
                    }
                    
                    let data: NSDictionary
                    do {
                        data = try NSJSONSerialization.JSONObjectWithData(responseData,
                            options: []) as! NSDictionary
                        average = (average + (data["last"]!.doubleValue))/2
                    } catch  {
                        print("error trying to convert data to JSON")
                        return
                    }
                    self.updateUI(data, average: average)
                    
                })
                task.resume()
            })
            btceTask.resume()
            
        })
        bitfinexTask.resume()
        return average
    }
    
    func updateUI(data: NSDictionary, average: Double) {
        self.fillChart()
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.beginUpdates()
            self.prijsLabel.text = "$" + String(format:"%.2f", average)
            let change = ((data["last"]!.doubleValue - data["vwap"]!.doubleValue)/data["last"]!.doubleValue) * 100
            self.changeLabel.text = String(format:"(%.2f%%)", change)
            if change < 0 {
                self.changeLabel.textColor = UIColor.redColor()
            } else {
                self.changeLabel.textColor = UIColor.greenColor()
            }
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
            self.updatedLabel.text = "Laatst bijgewerkt: " + dateFormatter.stringFromDate(NSDate())
            self.tableView.endUpdates()
            
        })
        tableView.reloadData()
        self.refreshControl?.endRefreshing()
        
    }
    
    func drawChart() {
        //chartView.delegate = self
        chartView.pinchZoomEnabled = true
        chartView.drawGridBackgroundEnabled = false
        chartView.backgroundColor = UIColor.init(red: (245/255), green: (143/255), blue: 0, alpha: 1)
        chartView.descriptionText = ""
        let yAxis: ChartYAxis = chartView.leftAxis
        yAxis.setLabelCount(6, force: false)
        yAxis.startAtZeroEnabled = false
        yAxis.labelTextColor = UIColor.whiteColor()
        yAxis.drawGridLinesEnabled = true
        yAxis.axisLineColor = UIColor.whiteColor()
        
        chartView.rightAxis.enabled = false;
        chartView.legend.enabled = false;
        chartView.xAxis.enabled = false;
        
        chartView.animate(xAxisDuration: 2, yAxisDuration: 2)
        
    }
    
    func fillChart() {
        let urlString: String = "https://blockchain.info/charts/market-price?format=json"
        guard let url = NSURL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = NSURLRequest(URL: url)
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            guard error == nil else {
                print("error calling GET on /charts")
                print(error)
                return
            }
            let data: NSDictionary
            do {
                data = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            
            let values: NSArray = data["values"] as! NSArray
            var yVals1: [ChartDataEntry] = [ChartDataEntry]()
            
            for index in 0...24
            {
                yVals1.append(ChartDataEntry.init())
                yVals1[index].value = Double(values[values.count - 20]["y"] as! Double)
            }
            
            let newSet: LineChartDataSet = LineChartDataSet.init(yVals: yVals1, label: "Dataset1")
            
            newSet.drawCubicEnabled = true
            newSet.cubicIntensity = 0.2
            newSet.drawCirclesEnabled = false
            newSet.lineWidth = 2
            newSet.circleRadius = 4
            newSet.setCircleColor(UIColor.whiteColor())
            newSet.highlightColor = UIColor.whiteColor()
            newSet.setColor(UIColor.whiteColor())
            newSet.fillColor = UIColor.whiteColor()
            newSet.fillAlpha = 1
            newSet.drawHorizontalHighlightIndicatorEnabled = true
            
            var i = 0
            for var index = values.count - 24; index < values.count; ++index {
                let entry: ChartDataEntry = ChartDataEntry()
                entry.value = Double(values[index]["y"] as! Double)
                entry.xIndex = i
                newSet.addEntry(entry)
                ++i
            }
            
            var xVals1: [String] = [String]()
            
            for var index = 0; index < 25; ++index {
                xVals1.append("\(index)")
            }
            
            let chartData: LineChartData = LineChartData.init(xVals: xVals1, dataSets: [newSet])
            chartData.setDrawValues(false)
            dispatch_async(dispatch_get_main_queue(), {
                self.chartView.data = chartData;
                self.chartView.reloadInputViews()
            })
            
        })
        task.resume()
    }
}