//
//  PriceDetailController.swift
//  Cointap
//
//  Created by Helder De Baere on 14/01/16.
//  Copyright © 2016 Helder De Baere. All rights reserved.
//

import UIKit

class PriceDetailController: UITableViewController {
    
    @IBOutlet weak var vraag: UILabel!
    @IBOutlet weak var bied: UILabel!
    @IBOutlet weak var hoog: UILabel!
    @IBOutlet weak var laag: UILabel!
    @IBOutlet weak var open: UILabel!
    @IBOutlet weak var volume: UILabel!
    @IBOutlet weak var gemiddeld: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let mainVC = self.storyboard!.instantiateViewControllerWithIdentifier("FirstViewController")
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        
//        UIView.transitionWithView(appDelegate.window!, duration: 0.5, options: .TransitionFlipFromLeft , animations: { () -> Void in
//            appDelegate.window!.rootViewController = mainVC
//            }, completion:nil)
        self.getData()
        self.refreshControl?.tintColor = UIColor.orangeColor()
        self.refreshControl?.addTarget(self, action: Selector("getData"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func getData() {
        let urlString: String = "https://www.bitstamp.net/api/ticker/"
        guard let url = NSURL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        
        let urlRequest = NSURLRequest(URL: url)
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: { (data, response, error) in
            
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            guard error == nil else {
                print("error calling GET on /ticker")
                print(error)
                return
            }
            
            let data: NSDictionary
            do {
                data = try NSJSONSerialization.JSONObjectWithData(responseData,
                    options: []) as! NSDictionary
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
            self.updateUI(data)
        })
        task.resume()
    }
    
    func updateUI(data: NSDictionary) {
        let fmt = NSNumberFormatter()
        fmt.numberStyle = .DecimalStyle
        dispatch_async(dispatch_get_main_queue(), {
            self.tableView.beginUpdates()
            self.vraag.text = String(format:"$%.2f", data["ask"]!.doubleValue)
            self.bied.text = String(format:"$%.2f", data["bid"]!.doubleValue)
            self.hoog.text = String(format:"$%.2f", data["high"]!.doubleValue)
            self.laag.text = String(format:"$%.2f", data["low"]!.doubleValue)
            self.open.text = String(format:"$%.2f", data["open"]!.doubleValue)
            self.volume.text = "Ƀ" + fmt.stringFromNumber(data["volume"]!.integerValue)!
            self.gemiddeld.text = String(format:"$%.2f", data["vwap"]!.doubleValue)
            self.tableView.endUpdates()
        })
        tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
}